const problem = require("../problem6.cjs")
const inventory = require("../inventory.cjs")

test('problem6 testing',() => {
    expect(problem(inventory, 'BMW','Audi')).toStrictEqual('[{"id":6,"car_make":"Audi","car_model":"riolet","car_year":1995},{"id":8,"car_make":"Audi","car_model":"4000CS Quattro","car_year":1987},{"id":25,"car_make":"BMW","car_model":"525","car_year":2005},{"id":30,"car_make":"BMW","car_model":"6 Series","car_year":2010},{"id":44,"car_make":"Audi","car_model":"Q7","car_year":2012},{"id":45,"car_make":"Audi","car_model":"TT","car_year":2008}]')
})

test('problem6 testing',() => {
    expect(problem(inventory, 'BMW', 45)).toStrictEqual('[{"id":25,"car_make":"BMW","car_model":"525","car_year":2005},{"id":30,"car_make":"BMW","car_model":"6 Series","car_year":2010}]')
})

test('problem6 testing',() => {
    expect(problem({})).toStrictEqual([])
})

test('problem6 testing',() => {
    expect(problem()).toStrictEqual([])
})

test('problem6 testing',() => {
    expect(problem('inventory')).toStrictEqual([])
})

test('problem6 testing',() => {
    expect(problem([])).toStrictEqual([])
})
