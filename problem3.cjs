// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const inventory = require("./inventory.cjs");

function problem3 (inventory) {
    if (arguments.length < 1 || !Array.isArray(inventory)) {
        return [];
    }
    if (inventory.length < 1) {
        return [];
    } else {
        inventory.sort(function(a, b){
            let x = a.car_make;
            let y = b.car_make;
            if (x < y) {return -1;}
            if (x > y) {return 1;}
            return 0;
          }); 
        
        return inventory;
    }
}

module.exports = problem3;